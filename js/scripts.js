/*jshint browser:true, jquery:true, white:false, smarttabs:true */
;(function ($) {
	'use strict';

	function _getContainers() {
		if (document.querySelectorAll) {
			return document.querySelectorAll('[data-component]');
		}
		else {
			var elems = document.getElementsByTagName('*'),
				containers = [];
			for (var elemIdx = 0, elemCnt = elems.length; elemIdx < elemCnt; elemIdx++) {
				var elem = elems[elemIdx];
				if (elem.getAttribute('data-component')) {
					containers.push(elem);
				}
			}
			return containers;
		}
	}
	var _containersCache;


	/**
	 * Initialize components
	 *
	 * @param {Object} funcs Initializers for each component: { pony: function(elem) { $(elem)... }, ... }
	 *
	 * <div data-component="pony"></div>
	 */
	function initComponents(funcs) {
		var containers = _containersCache || (_containersCache = _getContainers());
		for (var containerIdx = 0, containerCnt = containers.length; containerIdx < containerCnt; containerIdx++) {
			var container = containers[containerIdx],
				component = container.getAttribute('data-component');
			if (funcs[component]) {
				funcs[component](container);
			}
		}
	}


	/**
	 * Controls
	 *
	 * <span data-fire="slider-next" data-to=".portfolio" data-attrs="1,2,3">Next</span>
	 */
	$(document).click(function(e) {
		var target = e.target;
		if (target.getAttribute('data-fire') && target.getAttribute('data-to')) {
			target = $(target);
			var attrs = (''+target.data('attrs')).split(/[;, ]/);
			$(target.data('to')).trigger(target.data('fire'), attrs);
			e.preventDefault();
		}
	});


	window.utils = {
		initComponents: initComponents
	};
	
}(jQuery));

/*! Copyright (c) 2011 Brandon Aaron (/web/20150803041127/http://brandonaaron.net)
 * Licensed under the MIT License (LICENSE.txt).
 *
 * Thanks to: http://adomas.org/javascript-mouse-wheel/ for some pointers.
 * Thanks to: Mathias Bank(http://www.mathias-bank.de) for a scope bug fix.
 * Thanks to: Seamus Leahy for adding deltaX and deltaY
 *
 * Version: 3.0.6
 * 
 * Requires: 1.2.2+
 */

(function($) {

var types = ['DOMMouseScroll', 'mousewheel'];

if ($.event.fixHooks) {
    for ( var i=types.length; i; ) {
        $.event.fixHooks[ types[--i] ] = $.event.mouseHooks;
    }
}

$.event.special.mousewheel = {
    setup: function() {
        if ( this.addEventListener ) {
            for ( var i=types.length; i; ) {
                this.addEventListener( types[--i], handler, false );
            }
        } else {
            this.onmousewheel = handler;
        }
    },
    
    teardown: function() {
        if ( this.removeEventListener ) {
            for ( var i=types.length; i; ) {
                this.removeEventListener( types[--i], handler, false );
            }
        } else {
            this.onmousewheel = null;
        }
    }
};

$.fn.extend({
    mousewheel: function(fn) {
        return fn ? this.bind("mousewheel", fn) : this.trigger("mousewheel");
    },
    
    unmousewheel: function(fn) {
        return this.unbind("mousewheel", fn);
    }
});


function handler(event) {
    var orgEvent = event || window.event, args = [].slice.call( arguments, 1 ), delta = 0, returnValue = true, deltaX = 0, deltaY = 0;
    event = $.event.fix(orgEvent);
    event.type = "mousewheel";
    
    // Old school scrollwheel delta
    if ( orgEvent.wheelDelta ) { delta = orgEvent.wheelDelta/120; }
    if ( orgEvent.detail     ) { delta = -orgEvent.detail/3; }
    
    // New school multidimensional scroll (touchpads) deltas
    deltaY = delta;
    
    // Gecko
    if ( orgEvent.axis !== undefined && orgEvent.axis === orgEvent.HORIZONTAL_AXIS ) {
        deltaY = 0;
        deltaX = -1*delta;
    }
    
    // Webkit
    if ( orgEvent.wheelDeltaY !== undefined ) { deltaY = orgEvent.wheelDeltaY/120; }
    if ( orgEvent.wheelDeltaX !== undefined ) { deltaX = -1*orgEvent.wheelDeltaX/120; }
    
    // Add event and delta to the front of the arguments
    args.unshift(event, delta, deltaX, deltaY);
    
    return ($.event.dispatch || $.event.handle).apply(this, args);
}

})(jQuery);

/*!
 * jQuery throttle / debounce - v1.1 - 3/7/2010
 * http://benalman.com/projects/jquery-throttle-debounce-plugin/
 * 
 * Copyright (c) 2010 "Cowboy" Ben Alman
 * Dual licensed under the MIT and GPL licenses.
 * http://benalman.com/about/license/
 */

// Script: jQuery throttle / debounce: Sometimes, less is more!
//
// *Version: 1.1, Last updated: 3/7/2010*
// 
// Project Home - /web/20150803041127/http://benalman.com/projects/jquery-throttle-debounce-plugin/
// GitHub       - /web/20150803041127/http://github.com/cowboy/jquery-throttle-debounce/
// Source       - /web/20150803041127/http://github.com/cowboy/jquery-throttle-debounce/raw/master/jquery.ba-throttle-debounce.js
// (Minified)   - /web/20150803041127/http://github.com/cowboy/jquery-throttle-debounce/raw/master/jquery.ba-throttle-debounce.min.js (0.7kb)
// 
// About: License
// 
// Copyright (c) 2010 "Cowboy" Ben Alman,
// Dual licensed under the MIT and GPL licenses.
// /web/20150803041127/http://benalman.com/about/license/
// 
// About: Examples
// 
// These working examples, complete with fully commented code, illustrate a few
// ways in which this plugin can be used.
// 
// Throttle - /web/20150803041127/http://benalman.com/code/projects/jquery-throttle-debounce/examples/throttle/
// Debounce - /web/20150803041127/http://benalman.com/code/projects/jquery-throttle-debounce/examples/debounce/
// 
// About: Support and Testing
// 
// Information about what version or versions of jQuery this plugin has been
// tested with, what browsers it has been tested in, and where the unit tests
// reside (so you can test it yourself).
// 
// jQuery Versions - none, 1.3.2, 1.4.2
// Browsers Tested - Internet Explorer 6-8, Firefox 2-3.6, Safari 3-4, Chrome 4-5, Opera 9.6-10.1.
// Unit Tests      - /web/20150803041127/http://benalman.com/code/projects/jquery-throttle-debounce/unit/
// 
// About: Release History
// 
// 1.1 - (3/7/2010) Fixed a bug in <jQuery.throttle> where trailing callbacks
//       executed later than they should. Reworked a fair amount of internal
//       logic as well.
// 1.0 - (3/6/2010) Initial release as a stand-alone project. Migrated over
//       from jquery-misc repo v0.4 to jquery-throttle repo v1.0, added the
//       no_trailing throttle parameter and debounce functionality.
// 
// Topic: Note for non-jQuery users
// 
// jQuery isn't actually required for this plugin, because nothing internal
// uses any jQuery methods or properties. jQuery is just used as a namespace
// under which these methods can exist.
// 
// Since jQuery isn't actually required for this plugin, if jQuery doesn't exist
// when this plugin is loaded, the method described below will be created in
// the `Cowboy` namespace. Usage will be exactly the same, but instead of
// $.method() or jQuery.method(), you'll need to use Cowboy.method().

(function(window,undefined){
  '$:nomunge'; // Used by YUI compressor.
  
  // Since jQuery really isn't required for this plugin, use `jQuery` as the
  // namespace only if it already exists, otherwise use the `Cowboy` namespace,
  // creating it if necessary.
  var $ = window.jQuery || window.Cowboy || ( window.Cowboy = {} ),
    
    // Internal method reference.
    jq_throttle;
  
  // Method: jQuery.throttle
  // 
  // Throttle execution of a function. Especially useful for rate limiting
  // execution of handlers on events like resize and scroll. If you want to
  // rate-limit execution of a function to a single time, see the
  // <jQuery.debounce> method.
  // 
  // In this visualization, | is a throttled-function call and X is the actual
  // callback execution:
  // 
  // > Throttled with `no_trailing` specified as false or unspecified:
  // > ||||||||||||||||||||||||| (pause) |||||||||||||||||||||||||
  // > X    X    X    X    X    X        X    X    X    X    X    X
  // > 
  // > Throttled with `no_trailing` specified as true:
  // > ||||||||||||||||||||||||| (pause) |||||||||||||||||||||||||
  // > X    X    X    X    X             X    X    X    X    X
  // 
  // Usage:
  // 
  // > var throttled = jQuery.throttle( delay, [ no_trailing, ] callback );
  // > 
  // > jQuery('selector').bind( 'someevent', throttled );
  // > jQuery('selector').unbind( 'someevent', throttled );
  // 
  // This also works in jQuery 1.4+:
  // 
  // > jQuery('selector').bind( 'someevent', jQuery.throttle( delay, [ no_trailing, ] callback ) );
  // > jQuery('selector').unbind( 'someevent', callback );
  // 
  // Arguments:
  // 
  //  delay - (Number) A zero-or-greater delay in milliseconds. For event
  //    callbacks, values around 100 or 250 (or even higher) are most useful.
  //  no_trailing - (Boolean) Optional, defaults to false. If no_trailing is
  //    true, callback will only execute every `delay` milliseconds while the
  //    throttled-function is being called. If no_trailing is false or
  //    unspecified, callback will be executed one final time after the last
  //    throttled-function call. (After the throttled-function has not been
  //    called for `delay` milliseconds, the internal counter is reset)
  //  callback - (Function) A function to be executed after delay milliseconds.
  //    The `this` context and all arguments are passed through, as-is, to
  //    `callback` when the throttled-function is executed.
  // 
  // Returns:
  // 
  //  (Function) A new, throttled, function.
  
  $.throttle = jq_throttle = function( delay, no_trailing, callback, debounce_mode ) {
    // After wrapper has stopped being called, this timeout ensures that
    // `callback` is executed at the proper times in `throttle` and `end`
    // debounce modes.
    var timeout_id,
      
      // Keep track of the last time `callback` was executed.
      last_exec = 0;
    
    // `no_trailing` defaults to falsy.
    if ( typeof no_trailing !== 'boolean' ) {
      debounce_mode = callback;
      callback = no_trailing;
      no_trailing = undefined;
    }
    
    // The `wrapper` function encapsulates all of the throttling / debouncing
    // functionality and when executed will limit the rate at which `callback`
    // is executed.
    function wrapper() {
      var that = this,
        elapsed = +new Date() - last_exec,
        args = arguments;
      
      // Execute `callback` and update the `last_exec` timestamp.
      function exec() {
        last_exec = +new Date();
        callback.apply( that, args );
      };
      
      // If `debounce_mode` is true (at_begin) this is used to clear the flag
      // to allow future `callback` executions.
      function clear() {
        timeout_id = undefined;
      };
      
      if ( debounce_mode && !timeout_id ) {
        // Since `wrapper` is being called for the first time and
        // `debounce_mode` is true (at_begin), execute `callback`.
        exec();
      }
      
      // Clear any existing timeout.
      timeout_id && clearTimeout( timeout_id );
      
      if ( debounce_mode === undefined && elapsed > delay ) {
        // In throttle mode, if `delay` time has been exceeded, execute
        // `callback`.
        exec();
        
      } else if ( no_trailing !== true ) {
        // In trailing throttle mode, since `delay` time has not been
        // exceeded, schedule `callback` to execute `delay` ms after most
        // recent execution.
        // 
        // If `debounce_mode` is true (at_begin), schedule `clear` to execute
        // after `delay` ms.
        // 
        // If `debounce_mode` is false (at end), schedule `callback` to
        // execute after `delay` ms.
        timeout_id = setTimeout( debounce_mode ? clear : exec, debounce_mode === undefined ? delay - elapsed : delay );
      }
    };
    
    // Set the guid of `wrapper` function to the same of original callback, so
    // it can be removed in jQuery 1.4+ .unbind or .die by using the original
    // callback as a reference.
    if ( $.guid ) {
      wrapper.guid = callback.guid = callback.guid || $.guid++;
    }
    
    // Return the wrapper function.
    return wrapper;
  };
  
  // Method: jQuery.debounce
  // 
  // Debounce execution of a function. Debouncing, unlike throttling,
  // guarantees that a function is only executed a single time, either at the
  // very beginning of a series of calls, or at the very end. If you want to
  // simply rate-limit execution of a function, see the <jQuery.throttle>
  // method.
  // 
  // In this visualization, | is a debounced-function call and X is the actual
  // callback execution:
  // 
  // > Debounced with `at_begin` specified as false or unspecified:
  // > ||||||||||||||||||||||||| (pause) |||||||||||||||||||||||||
  // >                          X                                 X
  // > 
  // > Debounced with `at_begin` specified as true:
  // > ||||||||||||||||||||||||| (pause) |||||||||||||||||||||||||
  // > X                                 X
  // 
  // Usage:
  // 
  // > var debounced = jQuery.debounce( delay, [ at_begin, ] callback );
  // > 
  // > jQuery('selector').bind( 'someevent', debounced );
  // > jQuery('selector').unbind( 'someevent', debounced );
  // 
  // This also works in jQuery 1.4+:
  // 
  // > jQuery('selector').bind( 'someevent', jQuery.debounce( delay, [ at_begin, ] callback ) );
  // > jQuery('selector').unbind( 'someevent', callback );
  // 
  // Arguments:
  // 
  //  delay - (Number) A zero-or-greater delay in milliseconds. For event
  //    callbacks, values around 100 or 250 (or even higher) are most useful.
  //  at_begin - (Boolean) Optional, defaults to false. If at_begin is false or
  //    unspecified, callback will only be executed `delay` milliseconds after
  //    the last debounced-function call. If at_begin is true, callback will be
  //    executed only at the first debounced-function call. (After the
  //    throttled-function has not been called for `delay` milliseconds, the
  //    internal counter is reset)
  //  callback - (Function) A function to be executed after delay milliseconds.
  //    The `this` context and all arguments are passed through, as-is, to
  //    `callback` when the debounced-function is executed.
  // 
  // Returns:
  // 
  //  (Function) A new, debounced, function.
  
  $.debounce = function( delay, at_begin, callback ) {
    return callback === undefined
      ? jq_throttle( delay, at_begin, false )
      : jq_throttle( delay, callback, at_begin !== false );
  };
  
})(this);

/**
 * Gallery
 *
 * @version 0.0.1
 * @requires jQuery
 * @author Artem Sapegin
 * @copyright 2012 Artem Sapegin, /web/20150803041127/http://sapegin.me
 * @license MIT
 */

/*jshint browser:true, jquery:true, white:false, smarttabs:true, eqeqeq:true,
         immed:true, latedef:true, newcap:true, undef:true */
/*global define:false, Modernizr:false */
(function(factory) {  // Try to register as an anonymous AMD module
	if (typeof define === 'function' && define.amd) {
		define(['jquery'], factory);
	} else {
		factory(jQuery);
	}
}(function($) {
	'use strict';

	var TRANSITION_END_EVENT = {
		WebkitTransition : 'webkitTransitionEnd',
		MozTransition : 'transitionend',
		OTransition : 'otransitionend',
		msTransition : 'MSTransitionEnd',
		transition : 'transitionend'
	}[Modernizr.prefixed('transition')];

	$.fn.gallery = function(options) {
		options = $.extend({}, $.fn.gallery.defaults, options);

		return this.each(function() {
			new Gallery($(this), options);
		});
	};

	$.fn.gallery.defaults = {
		blockName: 'gallery',
		images: 'img',
		preloadCount: 3,
		counterTemplate: '<b>{current}</b> / {total}'
	};

	function Gallery(container, options) {
		this.container = container;
		this.options = options;

		this.init();
	}

	Gallery.prototype = {
		init: function() {
			this.images = this.container.find(this.options.images);
			this.titleElem = $(this.container.data('title'));
			this.counterElem = $(this.container.data('counter'));
			this.currentIndex = 0;
			
			this.createNavButton('prev');
			this.createNavButton('next');
			
			this.updateClasses();

			var that = this;
			this.container.on('click', '.js-gallery-nav', function(e) {
				that[$(e.target).data('direction')]();
			});
			this.container.on('gallery-set', function(e, i) {
				that.set(+i);
			});
		},

		createNavButton: function(direction) {
			var classes = [this.options.blockName + '__nav'];
			classes.push(classes[0] + '_' + direction, 'js-gallery-nav');
			var button = $('<div>', {
				'class': classes.join(' '),
				data: {
					direction: direction
				}
			});
			this.container.append(button);
		},

		next: function() {
			this.set(this.currentIndex + 1);
		},

		prev: function() {
			this.set(this.currentIndex - 1);
		},

		set: function(index) {
			if ((index === this.currentIndex) || (index >= this.images.length) || (index < 0)) return;

			var current = this.images.eq(this.currentIndex),
				next = this.images.eq(index),
				src = next.data('src');

			this.load(next);

			if (Modernizr.csstransitions && TRANSITION_END_EVENT) {
				// Stop unfinished animations
				this.images.off(TRANSITION_END_EVENT);
				current.siblings()
					.removeClass('is-shown')
					.css('display', 'none');

				next.insertAfter(current);
				next.css('display', 'block');
				setTimeout(function() {
					next.addClass('is-shown');
					next.one(TRANSITION_END_EVENT, function() {
						current.removeClass('is-shown');
					});
				}, 0);
			}
			else {
				this.images.stop();
				current.siblings().css('opacity', 0);
				current.css('opacity', 1);

				next.insertAfter(current);
				next.animate({opacity: 1}, function() {
					current.css('opacity', 0);
				});
			}

			if (this.titleElem.length && next.attr('title')) {
				this.titleElem.html(next.attr('title'));
			}
			if (this.counterElem.length && next.attr('title')) {
				this.counterElem.html(this.options.counterTemplate
					.replace('{current}', index + 1)
					.replace('{total}', this.images.length)
				);
			}

			this.currentIndex = index;
			this.updateClasses();
			this.preload();

			this.container.trigger('gallery-changed', index);
		},

		updateClasses: function() {
			this.container.toggleClass('is-gallery-last', this.currentIndex === this.images.length - 1);
			this.container.toggleClass('is-gallery-first', this.currentIndex === 0);
		},

		preload: function() {
			var begin = this.currentIndex + 1,
				end = Math.min(begin + this.options.preloadCount, this.images.length);
			for (var imageIdx = begin; imageIdx < end; imageIdx++) {
				this.load(this.images.eq(imageIdx));
			}
		},

		load: function(image) {
			var src = image.data('src');
			if (src) {
				image.attr('src', src);
				image.removeAttr('data-src');
			}
		}
	};

}));
/**
 * ScreenScroller
 *
 * @version 0.1.1
 * @requires jQuery, jQuery.mousewheel, Modernizr.cssvhunit
 * @author Artem Sapegin
 * @copyright 2012 Artem Sapegin, /web/20150803041127/http://sapegin.me
 * @license MIT
 */

/*jshint browser:true, jquery:true, white:false, smarttabs:true, eqeqeq:true,
         immed:true, latedef:true, newcap:true, undef:true */
/*global define:false, Modernizr:false */
(function(factory) {  // Try to register as an anonymous AMD module
	if (typeof define === 'function' && define.amd) {
		define(['jquery'], factory);
	} else {
		factory(jQuery);
	}
}(function($) {
	'use strict';

	var win = $(window),
		body = $('body');

	function ScreenScroller(container, options) {
		options = $.extend({}, ScreenScroller.defaults, options);

		this.container = container;
		this.options = options;

		this.init();
	}

	ScreenScroller.defaults = {
		sections: '.section',
		navbarBlock: 'navbar',
		buttonBlock: 'next-button',
		speed: 1500
	};

	ScreenScroller.prototype = {
		init: function() {
			this.sections = $(this.options.sections);
			this.makeNavbar();
			this.makeNextButton();
			this.setSizes();

			var current = $(location.hash);
			this.current = current.length ? current : this.sections.first();
			this.navigate(this.current, false);

			win.resize(this.setSizes.bind(this));
			var w = screen.width;
			if(w > 560){
			body.on('click', '.js-screenscroller-nav', this.navigate.bind(this));
			body.on('click', '.js-screenscroller-next', this.next.bind(this));
			body.on('mousewheel', $.throttle(4000, this.wheel.bind(this)));
			body.on('keydown', this.keydown.bind(this));
			}
		},

		makeNextButton: function() {
			var button = $('<div>', { 'class': this.options.buttonBlock });
			button.append($('<span>', {
				'class': this.options.buttonBlock + '__link js-screenscroller-next'
			}));
			body.append(button);
		},

		makeNavbar: function() {
			var block = this.options.navbarBlock,
				bar = $('<div>', {
					'class': block
				});

			this.sections.each(function() {
				var section = $(this);
				bar.append($('<a>', {
					'class': block + '__item js-screenscroller-nav',
					'href': '#' + section.attr('id'),
					'title': section.data('title'),
					'html': '<i></i>'
				}));
			});

			body.append(bar);
			this.navbar = bar;
		},

		setSizes: function() {
			//if (Modernizr.cssvhunit) return;
			this.sections.height($(window).height());
		},

		navigate: function(section, animate) {
			var hash;
			if (section.currentTarget) {
				section.preventDefault();
				var link = $(section.currentTarget);
				hash = link.attr('href');
				section = $(hash);
			}
			else {
				hash = '#' + section.attr('id');
			}

			var speed = animate === false ? 0 : this.options.speed;
			$('html, body').stop().animate({
				scrollTop: section.offset().top
			}, speed, function() {
				if (hash === '#home')
					this.removeHash();
				else
					location.hash = hash;
			}.bind(this));
			setTimeout(function() {
				this.updateNavbar();
				this.updateModifiers();
			}.bind(this), speed/2);

			this.current = section;
		},

		next: function(e) {
			var section = this.nextSection();
			this.navigate(section.length ? section : this.sections.first());
			e.preventDefault();
		},

		wheel: function(e, delta, deltaX, deltaY) {
			this.prevnext(deltaY < 0);
			e.preventDefault();
		},

		keydown: function(e) {
			var key = e.keyCode;
			if (key === 40 || key === 34) this.prevnext(true);
			if (key === 38 || key === 33) this.prevnext(false);
			if (key === 36) this.navigate(this.sections.first());
			if (key === 35) this.navigate(this.sections.last());
			e.preventDefault();
		},

		prevnext: function(next) {
			var section = this[next ? 'nextSection' : 'prevSection']();
			if (section.length) this.navigate(section);
		},

		removeHash: function() {
			if (history.pushState)
				history.pushState('', document.title, window.location.pathname + window.location.search);
		},

		updateNavbar: function() {
			var button = this.navbar.find('[href="#' + this.current.attr('id') + '"]');
			button.addClass('is-active');
			button.siblings().removeClass('is-active');
		},

		updateModifiers: function() {
			body[0].className = body[0].className.replace(/page_mod_\w+/g, '');  // Remove all mods

			// Color
			var mod = this.current.data('mod');
			if (mod) body.addClass('page_mod_' + mod);

			// Last
			if (!this.nextSection().length) body.addClass('page_mod_last');
		},

		prevSection: function() {
			return this.current.prev(this.options.sections);
		},

		nextSection: function() {
			return this.current.next(this.options.sections);
		}
};

	window.ScreenScroller = ScreenScroller;

}));
/* Author: Artem Sapegin, /web/20150803041127/http://sapegin.me, 2012 */

/*jshint browser:true, jquery:true, white:false, smarttabs:true, eqeqeq:true,
         immed:true, latedef:true, newcap:true, undef:true */
/*global utils:false, ScreenScroller:false */
;(function ($) {
	'use strict';

	new ScreenScroller();

	utils.initComponents({
		gallery: function(elem) {
			$(elem).gallery({
				blockName: 'gallery',
				images: '.gallery__img'
			});
		}
	});

}(jQuery));
